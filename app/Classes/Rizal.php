<?php

namespace App\Classes;

use App\Classes\Greeting;

class Rizal
{
    public Greeting $greeting;

    private string $name = 'Rizal';

    public function __construct(Greeting $greeting)
    {
        $this->greeting = $greeting;
    }

    public function sayHello(): string
    {
        return $this->greeting->greet($this->name);
    }
}
