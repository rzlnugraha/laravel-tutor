<?php

namespace App\Classes;

use App\Contracts\ICar;

class Avanza implements ICar
{
    public string $brand;
    public string $subBrand;
    public string $color;

    public function setBrand()
    {
        $this->brand = 'Avanza';
    }

    public function setSubBrand()
    {
        $this->subBrand = 'Toyota';
    }

    public function setColor()
    {
        $this->color = 'Black';
    }
}
