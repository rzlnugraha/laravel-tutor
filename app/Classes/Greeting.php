<?php

namespace App\Classes;

class Greeting 
{
    public function greet(string $name): string
    {
        return "Halo $name";
    }
}
