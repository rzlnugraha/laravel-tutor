<?php

namespace App\Contracts;

interface ICar
{
    public function setBrand();
    public function setSubBrand();
    public function setColor();
}