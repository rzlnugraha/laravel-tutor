<?php

namespace App\Providers;

use App\Classes\Avanza;
use App\Classes\Greeting;
use App\Classes\Rizal;
use App\Contracts\ICar;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class GreetingServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * One of how to create singleton concept
     */
    public array $singletons = [
        ICar::class => Avanza::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * DeferrableProvider make this provider will executed when class in provides function is called
         */
        dump('Greeting Service Provider');
        $this->app->singleton(Greeting::class, function ($app) {
            return new Greeting();
        });

        $this->app->singleton(Rizal::class, function ($app) {
            return new Rizal(resolve(Greeting::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            ICar::class,
            Greeting::class,
            Rizal::class
        ];
    }
}
