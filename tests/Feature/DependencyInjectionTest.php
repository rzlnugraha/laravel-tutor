<?php

namespace Tests\Feature;

use App\Classes\Greeting;
use App\Classes\Rizal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DependencyInjectionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDependencyInjection()
    {
        $greet = new Greeting();
        $rizal = new Rizal($greet);

        $this->assertEquals('Halo Wawat', $rizal->sayHello());
    }
}
