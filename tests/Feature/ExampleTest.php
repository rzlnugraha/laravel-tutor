<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function testFizzBuzz()
    {
        $result = '';
        for ($i=1; $i <= 100; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $result .= 'FizzBuzz';
                continue;
            } else if ($i % 3 == 0) {
                $result .= 'Fizz';
                continue;
            } else if ($i % 5 == 0) {
                $result .= 'Buzz';
                continue;
            }
            $result .= $i;

        }

        $this->assertSame('12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz1617Fizz19BuzzFizz2223FizzBuzz26Fizz2829FizzBuzz3132Fizz34BuzzFizz3738FizzBuzz41Fizz4344FizzBuzz4647Fizz49BuzzFizz5253FizzBuzz56Fizz5859FizzBuzz6162Fizz64BuzzFizz6768FizzBuzz71Fizz7374FizzBuzz7677Fizz79BuzzFizz8283FizzBuzz86Fizz8889FizzBuzz9192Fizz94BuzzFizz9798FizzBuzz', $result);
    }
}
