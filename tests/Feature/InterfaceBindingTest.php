<?php

namespace Tests\Feature;

use App\Classes\Avanza;
use App\Contracts\ICar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InterfaceBindingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testInterfaceBinding()
    {
        $this->app->singleton(ICar::class, Avanza::class);

        $avanza = resolve(ICar::class);
        $avanza->setBrand();

        $this->assertEquals('Avanza', $avanza->brand);
    }
}
