<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PalindromTest extends TestCase
{
    protected array $result;

    public function isPalindromTrue(string $text)
    {
        $split = str_split($text);
        $last = '';

        for ($i=count($split) - 1; $i >= 0; $i--) {
            $last .= $split[$i];
        }

        if ($text == $last) return $this->result[] = true;

        return $this->result[] = false;
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $text = ['aba', 'ccd', 'kodok', 'hasah'];

        foreach ($text as $value) {
            $this->isPalindromTrue($value);
        }

        $this->assertEquals([true, false, true, true], $this->result);
    }
}
