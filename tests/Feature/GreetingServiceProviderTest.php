<?php

namespace Tests\Feature;

use App\Classes\Greeting;
use App\Classes\Rizal;
use App\Contracts\ICar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GreetingServiceProviderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSingleton()
    {
        $greet = resolve(Greeting::class);
        $greet1 = resolve(Greeting::class);

        $this->assertSame($greet, $greet1);
        
        $rizal1 = resolve(Rizal::class);
        $rizal2 = resolve(Rizal::class);
        
        $this->assertSame($rizal1, $rizal2);

        $this->assertSame($greet, $rizal1->greeting);
        $this->assertSame($greet1, $rizal2->greeting);
    }

    public function testPropertySingleton()
    {
        $car = resolve(ICar::class);
        $car2 = resolve(ICar::class);

        $this->assertSame($car, $car2);
    }
}
