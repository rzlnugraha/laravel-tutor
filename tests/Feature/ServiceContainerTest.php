<?php

namespace Tests\Feature;

use App\Classes\Employee;
use App\Classes\Greeting;
use App\Classes\Rizal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class ServiceContainerTest extends TestCase
{
    /**
     * Testing for use service container laravel without singleton
     *
     * @return void
     */
    public function testServiceContainer()
    {
        /* You can use $this->app or resolve or app() */
        $greet1 = resolve(Greeting::class);
        $greet2 = resolve(Greeting::class);

        $this->assertEquals('Halo Rizal', $greet1->greet('Rizal'));
        $this->assertEquals('Halo Ijal', $greet1->greet('Ijal'));
        $this->assertNotSame($greet1, $greet2);
    }

    public function testBind()
    {
        /* Binding */
        $this->app->bind(Employee::class, function ($app, $params) {
            $firstName = $params[0] ?? 'Dummy';
            $lastName = $params[1] ?? 'Name';

            return new Employee($firstName, $lastName);
        });

        $employee1 = resolve(Employee::class, ['Rizal', 'Nugraha']);
        $employee2 = resolve(Employee::class);

        $this->assertEquals('Rizal', $employee1->firstName);
        $this->assertEquals('Dummy', $employee2->firstName);
        $this->assertNotSame($employee1, $employee2);
    }

    public function testSingleton()
    {
        /* Singleton */
        $this->app->singleton(Employee::class, function ($app, $params) {
            $firstName = $params[0] ?? 'Dummy';
            $lastName = $params[1] ?? 'Name';

            return new Employee($firstName, $lastName);
        });

        $employee1 = resolve(Employee::class);
        $employee2 = resolve(Employee::class);

        $this->assertEquals('Dummy', $employee1->firstName);
        $this->assertEquals('Dummy', $employee2->firstName);
        $this->assertSame($employee1, $employee2);
    }

    /**
     * Same with singleton concept
     */
    public function testInstance()
    {
        $employee = new Employee('Rizal', 'Nugraha');
        $this->app->instance(Employee::class, $employee);

        $employee1 = resolve(Employee::class);
        $employee2 = resolve(Employee::class);

        $this->assertSame($employee1, $employee2);
    }

    public function testDependencyInjectionBind()
    {
        $this->app->bind(Greeting::class, function ($app) {
            return new Greeting();
        });

        $rizal = resolve(Rizal::class);

        $this->assertEquals('Halo Rizal', $rizal->sayHello());
    }

    public function testDependencyInjectionSingleton()
    {
        $this->app->singleton(Greeting::class, function ($app) {
            return new Greeting();
        });

        $greet = resolve(Greeting::class);
        $rizal = resolve(Rizal::class);

        $this->assertEquals('Halo Rizal', $rizal->sayHello());
        $this->assertSame($greet, $rizal->greeting);
    }

    public function testDependencyInjectionClosure()
    {
        $this->app->singleton(Greeting::class, function ($app) {
            return new Greeting();
        });

        $this->app->singleton(Rizal::class, function ($app) {
            $greet = $app->make(Greeting::class);
            return new Rizal($greet);
        });

        $greet = resolve(Greeting::class);
        $rizal1 = resolve(Rizal::class);
        $rizal2 = resolve(Rizal::class);

        $this->assertSame($greet, $rizal1->greeting);
        $this->assertSame($rizal1, $rizal2);
    }
}
